import sys
import time
from optparse import OptionParser

import librosa
import numpy as np
import requests
from keras.models import load_model
from pydub import AudioSegment

from 预测 import get_wav
from 预测.utility import globalvars
from 预测.utility.audio import extract

try:
    import cPickle as pickle
except ImportError:
    import pickle

'''
Predict for one sample data
'''
if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-p', '--predicted_wav_path', dest='wav_path', default='/test.wav')
    parser.add_option('-m', '--model_path', dest='model_path', default='/weights_blstm_hyperas_1.h5')
    parser.add_option('-c', '--nb_classes', dest='nb_classes', type='int', default=7)

    (options, args) = parser.parse_args(sys.argv)

    # wav_path = options.wav_path
    # model_path = options.model_path
    # nb_classes = options.nb_classes

    # wav_path = 'wav/anger.wav'
    # wav_path = 'wav/disgust.wav'
    # wav_path = 'wav/boredom.wav'
    # wav_path = 'wav/sadness.wav'
    # wav_path = 'wav/neutral.wav'
    # wav_path = 'wav/fear.wav'
    # wav_path = 'wav/test.wav'
    model_path = 'model/weights_blstm_hyperas_4.h5'
    # model_path = '1.h5'
    nb_classes = 7

    globalvars.nb_classes = nb_classes

    # load model
    model = load_model(model_path)

    while True:
        # prediction
        get_wav.rec("wav/test.wav")
        wav_path = 'wav/test.wav'
        y, sr = librosa.load(wav_path, sr=16000)
        wav = AudioSegment.from_file(wav_path)
        f = extract(y, sr)
        u = np.full((f.shape[0], globalvars.nb_attention_param), globalvars.attention_init_value,
                    dtype=np.float32)
        results = model.predict([u, f], batch_size=128, verbose=1)
        judge = {0: 5, 1: 1, 2: 4, 3: 2, 4: 0, 5: 5, 6: 1}
        print(requests.post('http://localhost:8000/upload_record',
                            data={'score': judge[np.argmax(results)], 'type': 'audio'}))
        print(judge[np.argmax(results)])
        # for result in results:
        #     print(result)
        time.sleep(2)
