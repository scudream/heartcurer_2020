const app = getApp();
const util = require('../../utils/util.js');
Component({
  options: {
    addGlobalClass: true,
  },
  data: {
    recordTotal: 0,
    warningTotal: 0,
    userInfo: {}
  },
  attached() {
    let userInfo = wx.getStorageSync("userInfo");
    let that = this;
    wx.showLoading({
      title: '数据加载中',
      mask: true,
    })
    let i = 0;
    util.getRequest('/record/get_user_report?userId=' + wx.getStorageSync("userInfo").userId, res => {
      let tempNum = 0;
      for(let i = 0;i < res.data.data.length;i++){
        if(!res.data.data[i]['abnormal']){
          tempNum ++;
        }
      }
      that.setData({
        warningTotal: tempNum,
        recordTotal: res.data.data.length
      })
      console.log(res.data.data);
      numDH();
    }, res => {

    })
    

    function numDH() {
      if (i < 0) {
        setTimeout(function() {
          that.setData({
            warningTotal: i,
            recordTotal: i
          })
          i++
          numDH();
        }, 20)
      } else {
        that.setData({
          warningTotal: that.coutNum(that.data.warningTotal),
          recordTotal: that.coutNum(that.data.recordTotal)
        })
      }
    }
    wx.hideLoading()
    userInfo.avatar = app.globalData.domain + "/" + userInfo.avatar;
    this.setData({
      userInfo: userInfo
    })
  },
  methods: {
    coutNum(e) {
      if (e > 1000 && e < 10000) {
        e = (e / 1000).toFixed(1) + 'k'
      }
      if (e > 10000) {
        e = (e / 10000).toFixed(1) + 'W'
      }
      return e
    },
    CopyLink(e) {
      wx.setClipboardData({
        data: e.currentTarget.dataset.link,
        success: res => {
          wx.showToast({
            title: '已复制',
            duration: 1000,
          })
        }
      })
    },
    showModal(e) {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    },
    hideModal(e) {
      this.setData({
        modalName: null
      })
    },
    showQrcode() {
      wx.previewImage({
        urls: ['http://img.beihai365.com/bbs/photo/Mon_1709/205004_8b6815044605013b5c154a381b715.png?imageView2/2/w/750'],
        current: 'http://img.beihai365.com/bbs/photo/Mon_1709/205004_8b6815044605013b5c154a381b715.png?imageView2/2/w/750' // 当前显示图片的http链接      
      })
    },
    login: function() {

    }
  }
})