var wxCharts = require('../../../utils/wxcharts.js');
const util = require('../../../utils/util.js');
var app = getApp();
var lineChart = null;
let radarChart = null;
var graphData = null;
var currentIndex = 0;
Page({
  data: {
    modalName: '',
    domain: app.globalData.domain,
    imageUrl: 'logo.png'
  },
  touchHandler: function(e) {
    lineChart.showToolTip(e, {
      format: function(item, category) {
        return category + ' ' + item.name + ':' + item.data
      }
    });
    currentIndex = lineChart.getCurrentDataIndex(e);
    let item = graphData[lineChart.getCurrentDataIndex(e)];
    let windowWidth = 320;
    radarChart = new wxCharts({
      canvasId: 'radarCanvas',
      type: 'radar',
      categories: ['图像', '语音', '文本'],
      series: [{
        name: '情绪评分',
        data: [item.videoScore, item.audioScore, item.textScore]
      }],
      width: windowWidth,
      height: 200,
      extra: {
        radar: {
          max: Math.max(item.videoScore, item.audioScore, item.textScore)
        }
      }
    });
  },
  createSimulationData: function() {
    var categories = [];
    var data = [];
    for (var i = 0; i < 10; i++) {
      categories.push('2016-' + (i + 1));
      data.push(Math.random() * (20 - 10) + 10);
    }
    // data[4] = null;
    return {
      categories: categories,
      data: data
    }
  },
  updateData: function() {
    var simulationData = this.createSimulationData();
    var series = [{
      name: '成交量1',
      data: simulationData.data,
      format: function(val, name) {
        return val.toFixed(2) + '万';
      }
    }];
    lineChart.updateData({
      categories: simulationData.categories,
      series: series
    });
  },
  onLoad: function(e) {
    var windowWidth = 320;
    try {
      var res = wx.getSystemInfoSync();
      windowWidth = res.windowWidth;
    } catch (e) {
      console.error('getSystemInfoSync failed!');
    }
    util.getRequest('/record/get_user_report?userId=' + wx.getStorageSync('userInfo').userId, res => {
      let tempData = res.data.data;
      graphData = tempData;
      let resultData = [];
      let xData = [];
      for (let i = 0; i < tempData.length; i++) {
        resultData.push(tempData[i]['videoScore'] + tempData[i]['textScore'] + tempData[i]['audioScore']);
        xData.push(new Date(tempData[i]['date']).toLocaleDateString())
      }
      console.log(resultData);
      var simulationData = this.createSimulationData();
      lineChart = new wxCharts({
        canvasId: 'lineCanvas',
        type: 'line',
        categories: xData,
        animation: true,
        // background: '#f5f5f5',
        series: [{
          name: '实时评分',
          data: resultData,
          format: function(val, name) {
            return val.toFixed(2) + '分';
          }
        }],
        xAxis: {
          disableGrid: true
        },
        yAxis: {
          title: '情绪评分',
          format: function(val) {
            return val.toFixed(2);
          },
          min: 0
        },
        width: windowWidth,
        height: 200,
        dataLabel: false,
        dataPointShape: true,
        extra: {
          lineStyle: 'curve'
        }
      });
    }, res => {

    })
    var windowWidth = 320;
    try {
      var res = wx.getSystemInfoSync();
      windowWidth = res.windowWidth;
    } catch (e) {
      console.error('getSystemInfoSync failed!');
    }
    radarChart = new wxCharts({
      canvasId: 'radarCanvas',
      type: 'radar',
      categories: ['图像', '语音', '文本'],
      series: [{
        name: '情绪评分',
        data: [0, 0, 0]
      }],
      width: windowWidth,
      height: 200,
      extra: {
        radar: {
          max: 5
        }
      }
    });
  },
  showModal(e) {
    if (!graphData[currentIndex].hasOwnProperty("imageId")) {
      wx.showToast({
        title: '未找到图像',
        icon: 'none'
      })
      return;
    }
    let that = this;
    util.getRequest('/utils/getImageUrl?imageId=' + graphData[currentIndex].imageId, res => {
      let imageName = res.data.data;
      this.setData({
        imageUrl: imageName,
        modalName: e.currentTarget.dataset.target
      })
    }, res => {
      console.log(res);
    })

  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
});