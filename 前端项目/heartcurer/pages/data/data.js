// pages/admin/admin.js
Page({
  data: {
    iconList: [
    {
      icon: 'discoverfill',
      color: 'purple',
      badge: 0,
      name: '情绪趋势',
      url: 'line-chart'
    }, {
      icon: 'questionfill',
      color: 'mauve',
      badge: 0,
      name: '情绪分布状态',
      url:'pie-chart'
    }, {
      icon: 'commandfill',
      color: 'purple',
      badge: 0,
      name: '更多信息'
    }],
    gridCol: 3,
    skin: false
  },
})