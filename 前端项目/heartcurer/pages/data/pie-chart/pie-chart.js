var wxCharts = require('../../../utils/wxcharts.js');
const util = require('../../../utils/util.js');
var app = getApp();
let radarChart = null;

Page({
  data: {
    dataList: [],
    domain: app.globalData.domain,
    imageUrl: 'logo.png',
    modalName: null
  },
  onLoad: function() {
    let that = this;
    util.getRequest('/record/get_user_record?user_id=' + wx.getStorageSync('userInfo').userId, res => {
      console.log(res);
      let tempData = {};
      for (let i = 0; i < res.data.data.length; i++) {
        if (tempData.hasOwnProperty((res.data.data[i].videoScore + res.data.data[i].audioScore + res.data.data[i].textScore) + '分')) {
          tempData[(res.data.data[i].videoScore + res.data.data[i].audioScore + res.data.data[i].textScore) + '分'] += 1;
        } else {
          tempData[(res.data.data[i].videoScore + res.data.data[i].audioScore + res.data.data[i].textScore) + '分'] = 1;
        }
      }
      that.setData({
        dataList: res.data.data
      })
      var windowWidth = 320;
      radarChart = new wxCharts({
        canvasId: 'radarCanvas',
        type: 'radar',
        categories: Object.keys(tempData),
        series: [{
          name: '情绪评分分布',
          data: Object.values(tempData)
        }],
        width: windowWidth,
        height: 200,
        extra: {
          radar: {
            max: 5
          }
        }
      });
    }, res => {

    })
  },
  showModal(e) {
    console.log(e);
    if (!e.currentTarget.dataset.info.hasOwnProperty("imageId")) {
      wx.showToast({
        title: '未找到图像',
        icon: 'none'
      })
      return;
    }
    let that = this;
    this.setData({
      imageUrl: e.currentTarget.dataset.info.imageId,
      modalName: 'Image'
    })

  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
})