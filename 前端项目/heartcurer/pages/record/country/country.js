import * as echarts from '../../../ec-canvas/echarts';
import geoJson from './mapData.js';
const util = require('../../../utils/util.js');

const app = getApp();

function initChart(canvas, width, height, dpr) {
  let data = [];
  let max = 0;
  util.getRequest('/record/province_record', res => {
    console.log(res);
    data = res.data.data;
    for (let i = 0; i < data.length; i++) {
      if(data[i]['recordNum'] > max){
        max = data[i]['recordNum'];
      }
      data[i]['name'] = data[i]['provinceName'];
      data[i]['value'] = data[i]['recordNum'];
    }
    console.log(data);

    const chart = echarts.init(canvas, null, {
      width: width,
      height: height,
      devicePixelRatio: dpr // new
    });
    canvas.setChart(chart);

    echarts.registerMap('china', geoJson);
    const option = {
      tooltip: {
        trigger: 'item',
        formatter: '{b}: {c}'
      },

      visualMap: {
        min: 0,
        max: max,
        left: 'left',
        top: 'bottom',
        text: ['高风险', '低风险'], // 文本，默认为数值文本
        calculable: true
      },
      toolbox: {
        show: true,
        orient: 'vertical',
        left: 'right',
        top: 'center',
        feature: {
          dataView: {
            readOnly: false
          },
          restore: {},
          saveAsImage: {}
        }
      },
      series: [{
        type: 'map',
        mapType: 'china',
        label: {
          normal: {
            show: true
          },
          emphasis: {
            textStyle: {
              color: '#fff'
            }
          }
        },
        itemStyle: {

          normal: {
            borderColor: '#389BB7',
            areaColor: '#fff',
          },
          emphasis: {
            areaColor: '#389BB7',
            borderWidth: 0
          }
        },
        animation: false,

        data: data

      }],

    };

    chart.setOption(option);
    return chart;
  }, res => {
    wx.showToast({
      title: '加载数据失败！',
      icon: 'none'
    })
    retrun
  })
}
Page({
  data: {
    ec: {
      onInit: initChart
    }
  },
})