const chart = require('../../../utils/wxcharts.js');
const util = require('../../../utils/util.js');
var app = getApp();
var pieChart = null;
Page({
  data: {
    reportData: [],
    dataList: [],
    domain: app.globalData.domain
  },
  tabSelect: function(e) {
    console.log(e);
    this.setData({
      TabCur: e.currentTarget.dataset.id
    })
  },
  onLoad: function(e) {
    let that = this;
    util.getRequest('/record/score_record', res => {
      let temp = res.data.data;
      for (let i = 0; i < temp.length; i++) {
        temp[i]['name'] = temp[i]['score'] + '分';
        temp[i]['data'] = temp[i]['recordNum'];
      }
      that.setData({
        reportData: temp
      })
      that.filter(temp, 0);
      var windowWidth = 320;
      try {
        var res = wx.getSystemInfoSync();
        windowWidth = res.windowWidth;
      } catch (e) {
        console.error('getSystemInfoSync failed!');
      }
      console.log(temp);
      pieChart = new chart({
        animation: true,
        canvasId: 'pieCanvas',
        type: 'pie',
        series: that.filter(temp, 0),
        width: windowWidth,
        height: 300,
        dataLabel: true,
      });
      that.refreshList(this.data.reportData[0]['score']);
    }, res => {

    })

  },
  refreshList: function(score) {
    let that = this;
    util.getRequest('/record/score_record_by_score?score=' + score, res => {
      console.log(res);
      that.setData({
        dataList: res.data.data
      })
    }, res => {

    })
  },
  filter: function(data, index) {
    let tempData = [];
    for (let item in data) {
      if (data[item]['recordNum'] > index * 6 && data[item]['recordNum'] < (index + 1) * 6)
        tempData.push(data[item])
    }
    return tempData;
  },
  touchHandler: function(e) {
    let tempItem = this.data.reportData[pieChart.getCurrentDataIndex(e)];
    console.log(tempItem);
    let that = this;
    util.getRequest('/record/score_record_by_score?score=' + tempItem.score, res => {
      that.setData({
        dataList: res.data.data
      })
    }, res => {

    })
  }
})