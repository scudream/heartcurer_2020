const util = require('../../../utils/util.js');
const app = getApp();
Page({
  data: {
    userList: {},
    domain: app.globalData.domain
  },
  onLoad: function() {
    let that = this;
    util.getRequest('/user/get_all_users', res => {
      console.log(res)
      that.setData({
        userList: res.data.data
      })
    }, res => {
      console.log(res)
    })
  },
  // ListTouch触摸开始
  ListTouchStart(e) {
    this.setData({
      ListTouchStart: e.touches[0].pageX
    })
  },

  // ListTouch计算方向
  ListTouchMove(e) {
    this.setData({
      ListTouchDirection: e.touches[0].pageX - this.data.ListTouchStart > 0 ? 'right' : 'left'
    })
  },

  // ListTouch计算滚动
  ListTouchEnd(e) {
    if (this.data.ListTouchDirection == 'left') {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    } else {
      this.setData({
        modalName: null
      })
    }
    this.setData({
      ListTouchDirection: null
    })
  },
  deleteUser: function(event){
    let that = this;
    let userId = event.currentTarget.dataset.target;

    console.log(event);
    wx.request({
      url: app.globalData.domain + '/user/delete_user?user_id=' + userId,
      method: 'DELETE',
      success: res => {
        console.log(res);
        that.onLoad();
        wx.showToast({
          title: '删除成功!',
        });
      }
    })
  },
  viewDetail: function(e){
    console.log(e)
    wx.navigateTo({
      url: '/pages/record/user_record/user_detail/user_detail?userId=' + e.currentTarget.dataset.userid,
    });
  }
})