const app = getApp();
const util = require('../../../../utils/util.js');
Page({
  data: {
    userInfo: {},
    domain: app.globalData.domain
  },
  onLoad: function(e) {
    let that = this;
    console.log(e);
    util.getRequest('/user/get_user_info?user_id=' + e.userId, res => {
      console.log(res)
      res.data.data.registerDate = new Date(res.data.data.registerDate).toLocaleDateString();
      that.setData({
        userInfo: res.data.data
      })
    }, res => {
      console.log(res)
    })
  }

})