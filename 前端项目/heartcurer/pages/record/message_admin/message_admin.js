const app = getApp();
const util = require('../../../utils/util.js');
Page({
  data: {
    dataList: [],
    domain: app.globalData.domain
  },
  onLoad: function() {
    let that = this;
    util.getRequest('/message/getAllMessage', res => {
      console.log(res);
      that.setData({
        dataList: res.data.data
      })
    }, res => {
    })
  }
})