const app = getApp();
const util = require("../../../utils/util.js");
Page({
  login: function(e) {
    util.postRequest('/user/login', e.detail.value, res => {
      if (res.data.msg == 'fail') {
        wx.showToast({
          title: '登陆失败!',
          icon: 'none'
        });
        return;
      }
      wx.setStorageSync("userInfo", res.data.data);
      wx.showToast({
        title: '登陆成功!',
      });
      wx.navigateTo({
        url: '/pages/index/index',
      });
      console.log(res);
    }, res => {
      wx.showToast({
        icon: 'none',
        title: '登陆失败!',
      });
    })
  }
})