const util = require('../../../utils/util.js');
const app = getApp();
Page({

  data: {
    messageList: {}
  },
  onLoad: function() {
    let userInfo = wx.getStorageSync('userInfo');
    let that = this;
    util.getRequest('/message/getMessage?user_id=' + userInfo.userId, res => {
      let tempList = [];
      for (let i = 0; i < res.data.data.length; i++) {
        let item = res.data.data[i];
        item.date = new Date(item.date).toLocaleString();
        tempList.push(item);
      }
      that.setData({
        messageList: tempList
      })
      console.log(res);
    }, res => {
      console.log(res)
    })
  }, 
  // ListTouch触摸开始
  ListTouchStart(e) {
    this.setData({
      ListTouchStart: e.touches[0].pageX
    })
  },

  // ListTouch计算方向
  ListTouchMove(e) {
    this.setData({
      ListTouchDirection: e.touches[0].pageX - this.data.ListTouchStart > 0 ? 'right' : 'left'
    })
  },

  // ListTouch计算滚动
  ListTouchEnd(e) {
    if (this.data.ListTouchDirection == 'left') {
      this.setData({
        modalName: e.currentTarget.dataset.target
      })
    } else {
      this.setData({
        modalName: null
      })
    }
    this.setData({
      ListTouchDirection: null
    })
  },
  deleteMessage: function(e){
    let messageId = e.currentTarget.dataset.id;
    let that = this;
    wx.request({
      url: app.globalData.domain + '/message/deleteMessage?message_id=' + messageId,
      method: 'DELETE',
      success: res => {
        console.log(res);
        that.onLoad();
      }
    })
  }
})