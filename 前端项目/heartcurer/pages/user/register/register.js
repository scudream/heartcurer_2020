// pages/user/register/register.js
const util = require('../../../utils/util.js');
const app = getApp();
Page({
  data: {
    region: ['广东省', '广州市', '海珠区'],
    imgList: [],
    imageId: '',
    passCaptcha: false
  },
  register: function(e) {
    location = this.data.region.join('') + e.detail.value.location
    let requestData = e.detail.value;
    requestData['avatar'] = this.data.imageId;
    requestData['location'] = location;
    util.postRequest('/user/register', requestData, res => {
      console.log(res);
      console.log(requestData);
      wx.showToast({
        title: '注册成功!',
        icon: 'none'
      });
      wx.navigateTo({
        url: '/pages/index/index',
      });
    }, res => {
      console.log(res);
      wx.showToast({
        title: '注册失败!',
        icon: 'none'
      });
      return;
    })
  },
  RegionChange: function(e) {
    this.setData({
      region: e.detail.value
    })
  },
  ChooseImage() {
    let that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        util.uploadFile('/utils/uploadImage', res.tempFilePaths[0], r => {
          that.setData({
            imageId: r.data
          });
          console.log(r);
        }, res => {
          console.log(res);
          wx.showToast({
            title: '上传失败!',
            icon: 'none'
          });
        })
        if (this.data.imgList.length != 0) {
          this.setData({
            imgList: this.data.imgList.concat(res.tempFilePaths)
          })
        } else {
          this.setData({
            imgList: res.tempFilePaths
          })
        }
      }
    });
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '删除照片',
      content: '确定要删除这张照片吗？',
      cancelText: '再看看',
      confirmText: '再见',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList
          })
        }
      }
    })
  },
})