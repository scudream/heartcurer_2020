const util = require('../../../utils/util.js');
Page({
  data: {
    recordList: {}
  },
  onLoad: function() {
    let userInfo = wx.getStorageSync('userInfo');
    let that = this;
    util.getRequest('/login_record/get_records?user_id=' + userInfo.userId, res => {
      let tempList = [];
      for(let i = 0;i < res.data.data.length;i++){
        let item = res.data.data[i];
        item.date = new Date(item.date).toLocaleString();
        tempList.push(item);
      }
      that.setData({
        recordList: tempList
      })
      console.log(res);
    }, res => {
      console.log(res)
    })
  }
})