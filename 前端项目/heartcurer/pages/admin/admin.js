// pages/admin/admin.js
Page({
  data: {
    isAdmin: wx.getStorageSync("userInfo").admin,
    iconList: [{
      icon: 'cardboardfill',
      color: 'red',
      name: '用户管理',
      url: 'user_record'
    }, {
      icon: 'recordfill',
      color: 'orange',
      badge: 1,
      name: '记录统计',
      url: 'score_record'
    }, {
      icon: 'noticefill',
      color: 'olive',
      badge: 22,
      name: '消息管理',
      url: 'message_admin'
    }, {
      icon: 'upstagefill',
      color: 'cyan',
      badge: 0,
      name: '全国形势',
      url: 'country'
    }, {
      icon: 'discoverfill',
      color: 'purple',
      badge: 0,
      name: '发现'
    }, {
      icon: 'questionfill',
      color: 'mauve',
      badge: 0,
      name: '帮助'
    }, {
      icon: 'commandfill',
      color: 'purple',
      badge: 0,
      name: '问答'
    }],
    gridCol: 3,
    skin: false
  },
})