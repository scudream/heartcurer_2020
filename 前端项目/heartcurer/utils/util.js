const app = getApp()
module.exports = {
  postRequest: postRequest,
  uploadFile: uploadFile,
  getRequest: getRequest
}

function postRequest(url, data, successHandler, failHandler) {
  wx.request({
    url: app.globalData.domain + url,
    method: 'POST',
    data: data,
    success: successHandler,
    fail: failHandler
  })
}

function uploadFile(url, filePath, successHandler, failHandler) {
  wx.uploadFile({
    url: app.globalData.domain + url,
    filePath: filePath,
    name: 'file',
    success: successHandler,
    fail: failHandler
  })
}

function getRequest(url, successHandler, failHandler){
  wx.request({
    url: app.globalData.domain + url,
    success: successHandler,
    fail: failHandler
  })
}

